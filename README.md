<!-- @file Project Page -->
# BADCamp Sight

This theme serves as the main theme used for BADCamp. The underlying CSS framework is [Foundation 6](https://foundation.zurb.com/sites.html).

### Features
TBD

### Documentation
TBD

### Supported Modules
TBD
